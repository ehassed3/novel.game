import * as React from 'react';
import Head from 'next/head';

import MuiContainer from '@mui/material/Container';
import MuiTypography from '@mui/material/Typography';

const IndexPage: React.FC = () => (
  <>
    <Head>
      <title>Novel Game — Index Page</title>
      <meta name="description" content="Novel Game — Index Page" />
    </Head>
    <MuiContainer
      component="main"
      sx={{
        display: 'flex',
        flexDirection: { mobile: 'column', md: 'row' },
        alignItems: 'center',
        bgcolor: 'background.paper',
        overflow: 'hidden',
        borderRadius: '12px',
        boxShadow: 1,
        fontWeight: 'bold',
      }}
    >
      <MuiTypography>Index Page</MuiTypography>
    </MuiContainer>
  </>
);

export default IndexPage;
