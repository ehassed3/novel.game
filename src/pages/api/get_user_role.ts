import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/react';
import { User } from 'next-auth';

import db from '@middlewares/db';

import ROLE from '@constants/role';

const handler = nextConnect();

handler.use(db);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });

  if (!session || !session.user) {
    return res.status(200).json(ROLE.UNKNOWN_USER);
  }

  const author = await req.db
    .collection<User>('authors')
    .findOne({ email: session.user.email });

  if (!author) {
    return res.status(200).json(ROLE.USER);
  }

  return res.status(200).json(ROLE.AUTHOR);
});

export default handler;
