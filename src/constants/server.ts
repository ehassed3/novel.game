const SERVER =
  process.env.NODE_ENV !== 'production'
    ? 'http://localhost:3000'
    : 'https://novel.game';

export default SERVER;
