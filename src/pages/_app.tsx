import * as React from 'react';
import { AppProps } from 'next/app';
import { SessionProvider } from 'next-auth/react';
import { SWRConfig } from 'swr';
import { motion, AnimatePresence } from 'framer-motion';

import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';

import { CacheProvider, EmotionCache } from '@emotion/react';
import createCache from '@emotion/cache';

import Layout from '@components/Layout';

import THEME from '@constants/theme';

const clientSideEmotionCache = createCache({ key: 'css', prepend: true });

interface NovelGameAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

const NovelGameApp: React.FC<NovelGameAppProps> = ({
  Component,
  emotionCache = clientSideEmotionCache,
  pageProps,
  router,
}) => {
  const [shouldComponentUpdate, setShouldComponentUpdate] =
    React.useState(true);
  const [isVisibleComponent, setIsVisibleComponent] = React.useState(true);
  const [timedOut, setTimedOut] = React.useState(true);

  React.useEffect(() => {
    window.history.scrollRestoration = 'manual';

    router.events.on('routeChangeStart', (_, { shallow }) => {
      if (shallow || !isVisibleComponent) {
        return;
      }

      setIsVisibleComponent(false);
      setTimedOut(false);
    });

    router.events.on('routeChangeComplete', () => {
      setTimedOut(true);
    });
  }, []);

  const ComponentRef = React.useRef(<Component {...pageProps} />);

  React.useEffect(() => {
    if (!shouldComponentUpdate) {
      setShouldComponentUpdate(true);
      return;
    }

    ComponentRef.current = <Component {...pageProps} />;
  }, [Component]);

  return (
    <SWRConfig
      value={{
        fetcher: (input, init) => fetch(input, init).then((res) => res.json()),
      }}
    >
      <SessionProvider session={pageProps.session}>
        <CacheProvider value={emotionCache}>
          <ThemeProvider theme={THEME}>
            <CssBaseline />
            <Layout>
              <AnimatePresence
                initial={false}
                onExitComplete={() => {
                  setIsVisibleComponent(true);
                  window.scrollTo({ top: 0 });
                }}
              >
                {isVisibleComponent && timedOut && (
                  <motion.main
                    initial={{ opacity: 0 }}
                    animate={{
                      opacity: 1,
                      transition: { duration: 0.5 },
                    }}
                    exit={{ opacity: 0, transition: { duration: 0.4 } }}
                  >
                    {ComponentRef.current}
                  </motion.main>
                )}
              </AnimatePresence>
            </Layout>
          </ThemeProvider>
        </CacheProvider>
      </SessionProvider>
    </SWRConfig>
  );
};

export default NovelGameApp;
