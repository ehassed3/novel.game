import * as React from 'react';
import useSWR from 'swr';

import MuiTypography from '@mui/material/Typography';
import MuiBox from '@mui/material/Box';

import { useApp } from '@context/app';

import Chapter from '@interfaces/chapter';

const Chapter: React.FC = () => {
  const app = useApp();

  const { data: currentChapter } = useSWR<Chapter>(
    app.data && app.progress
      ? `/api/get_chapter?app=${app.data.name}&page=${app.progress.chapterNumber}`
      : ''
  );
  const { data: nextChapter } = useSWR<Chapter>(
    app.data && app.progress
      ? `/api/get_chapter?app=${app.data.name}&page=${
          app.progress.chapterNumber + 1
        }`
      : ''
  );

  return (
    <MuiBox>
      <MuiTypography>{currentChapter?.number}</MuiTypography>
      <MuiTypography>{nextChapter?.number}</MuiTypography>
    </MuiBox>
  );
};

export default Chapter;
