import * as React from 'react';
import { GetStaticPaths, GetStaticProps } from 'next';
import ErrorPage from 'next/error';
import Head from 'next/head';
import Image from 'next/image';

import MuiContainer from '@mui/material/Container';
import MuiTypography from '@mui/material/Typography';

import SERVER from '@constants/server';

import Preview from '@interfaces/preview';

interface Props {
  preview: Preview | null;
}

const PreviewAppPage: React.FC<Props> = ({ preview }) =>
  preview ? (
    <>
      <Head>
        <title>Novel Game — {preview.title}</title>
        <meta name="description" content={`Novel Game — ${preview.title}`} />
      </Head>
      <MuiContainer
        component="main"
        sx={{
          display: 'flex',
          flexDirection: { mobile: 'column', md: 'row' },
          alignItems: 'center',
          bgcolor: 'background.paper',
          overflow: 'hidden',
          borderRadius: '12px',
          boxShadow: 1,
          fontWeight: 'bold',
        }}
      >
        <Image
          alt={`${preview.title} image`}
          src={preview.image}
          layout="fill"
          objectFit="cover"
        />
        <MuiTypography>{preview.title}</MuiTypography>
        <MuiTypography>{preview.description}</MuiTypography>
      </MuiContainer>
    </>
  ) : (
    <ErrorPage statusCode={404} />
  );

export const getStaticPaths: GetStaticPaths = async () => {
  // const resApps = await fetch(`${SERVER}/api/get_apps/`);
  // const apps: string[] = resApps.ok ? await resApps.json() : [];
  // return {
  //   paths: apps.map((name) => ({ params: { name } })),
  //   fallback: false,
  // };

  return {
    paths: [],
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const resPreview = await fetch(
    `${SERVER}/api/get_preview_app?name=${params?.name}`
  );
  const preview: Preview | null = resPreview.ok
    ? await resPreview.json()
    : null;

  return { props: { preview } };
};

export default PreviewAppPage;
