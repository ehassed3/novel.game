const getCurrencySign = (currencyCode: string): string => {
  switch (currencyCode) {
    case 'USD':
      return '$';
    case 'EUR':
      return '€';
    case 'RUB':
      return '₽';
    default:
      return currencyCode;
  }
};

export default getCurrencySign;
