interface Protagonist {
  description: string;
  image: string;
  name: string;
}

export default Protagonist;
