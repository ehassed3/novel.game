import * as React from 'react';

import MuiButton from '@mui/material/Button';
import { SxProps, Theme } from '@mui/material/styles';

import LogoSymbol from '@icons/Logo/Symbol';

interface Props {
  sx?: SxProps<Theme>;
}

const Logo: React.FC<Props> = ({ sx = [] }) => (
  <MuiButton
    sx={[
      {
        height: { xs: '60px', sm: '68px' },
        overflow: 'hidden',
      },
      ...(Array.isArray(sx) ? sx : [sx]),
    ]}
    href="/"
  >
    <LogoSymbol
      sx={{
        height: { xs: '60px', md: '68px' },
        width: { xs: '120px', md: '162px' },
      }}
    />
  </MuiButton>
);

export default Logo;
