import * as React from 'react';
import { GetStaticPaths, GetStaticProps } from 'next';
import ErrorPage from 'next/error';
import Head from 'next/head';

import MuiContainer from '@mui/material/Container';
import MuiTypography from '@mui/material/Typography';

import SERVER from '@constants/server';

import Author from '@interfaces/author';

interface Props {
  author: Author | null;
}

const AuthorPage: React.FC<Props> = ({ author }) =>
  author ? (
    <>
      <Head>
        <title>Novel Game — {author.title}</title>
        <meta name="description" content={`Novel Game — ${author.title}`} />
      </Head>
      <MuiContainer
        component="main"
        sx={{
          display: 'flex',
          flexDirection: { mobile: 'column', md: 'row' },
          alignItems: 'center',
          bgcolor: 'background.paper',
          overflow: 'hidden',
          borderRadius: '12px',
          boxShadow: 1,
          fontWeight: 'bold',
        }}
      >
        <MuiTypography>{author.title}</MuiTypography>
        <MuiTypography>{author.description}</MuiTypography>
      </MuiContainer>
    </>
  ) : (
    <ErrorPage statusCode={404} />
  );

export const getStaticPaths: GetStaticPaths = async () => {
  // const resAuthors = await fetch(`${SERVER}/api/get_authors/`);
  // const authors: string[] = resAuthors.ok ? await resAuthors.json() : [];

  // return {
  //   paths: authors.map((name) => ({ params: { name } })),
  //   fallback: false,
  // };

  return {
    paths: [],
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const resAuthor = await fetch(
    `${SERVER}/api/get_author?name=${params?.name}`
  );
  const author: Author | null = resAuthor.ok ? await resAuthor.json() : null;

  return { props: { author } };
};

export default AuthorPage;
