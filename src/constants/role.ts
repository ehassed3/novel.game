enum ROLE {
  ADMIN = 'admin',
  AUTHOR = 'author',
  UNKNOWN_USER = 'uknown_user',
  USER = 'user',
}

export default ROLE;
