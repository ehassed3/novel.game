interface Character {
  description: string;
  image: string;
  name: string;
  relation: number;
}

export default Character;
