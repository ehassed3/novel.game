import * as React from 'react';
import { useRouter } from 'next/router';

import Chapter from '@components/Chapter';

import { AppProvider } from '@context/app';

const AppPage: React.FC = () => {
  const router = useRouter();
  const { app } = router.query;

  return (
    <AppProvider name={app?.toString()}>
      <Chapter />
    </AppProvider>
  );
};

export default AppPage;
