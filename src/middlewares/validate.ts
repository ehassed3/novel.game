import Ajv, { JSONSchemaType } from 'ajv';
import { NextApiRequest, NextApiResponse } from 'next';
import { NextHandler } from 'next-connect';

const validate = (schema: JSONSchemaType<Record<string, unknown>>) => {
  const ajv = new Ajv();
  const validate = ajv.compile(schema);

  return (req: NextApiRequest, res: NextApiResponse, next: NextHandler) => {
    const valid = validate(req.body);

    if (valid) {
      return next();
    }

    const error = validate.errors && validate.errors[0];
    return res.status(400).json({
      error: {
        message: error
          ? `"${error.instancePath.substring(1)}" ${error.message}`
          : 'Unknown validation error',
      },
    });
  };
};

export default validate;
