import { NextApiRequest, NextApiResponse } from 'next';
import NextAuth from 'next-auth';
import { MongoDBAdapter } from '@next-auth/mongodb-adapter';
import EmailProvider from 'next-auth/providers/email';
import GoogleProvider from 'next-auth/providers/google';
import MailruProvider from 'next-auth/providers/mailru';
import FacebookProvider from 'next-auth/providers/facebook';
import VkProvider from 'next-auth/providers/vk';
import TwitterProvider from 'next-auth/providers/twitter';

import { clientPromise } from '@lib/dbadapter';

import SERVER from '@constants/server';
import ROLE from '@constants/role';

const Auth = (req: NextApiRequest, res: NextApiResponse) =>
  NextAuth(req, res, {
    theme: {
      colorScheme: 'dark',
    },
    adapter: MongoDBAdapter(clientPromise),
    providers: [
      EmailProvider({
        server: process.env.EMAIL_SERVER,
        from: process.env.EMAIL_FROM,
      }),
      GoogleProvider({
        clientId: process.env.GOOGLE_ID,
        clientSecret: process.env.GOOGLE_SECRET,
      }),
      MailruProvider({
        clientId: process.env.MAILRU_ID,
        clientSecret: process.env.MAILRU_SECRET,
      }),
      FacebookProvider({
        clientId: process.env.FACEBOOK_ID,
        clientSecret: process.env.FACEBOOK_SECRET,
      }),
      VkProvider({
        clientId: process.env.VK_ID,
        clientSecret: process.env.VK_SECRET,
      }),
      TwitterProvider({
        clientId: process.env.TWITTER_ID,
        clientSecret: process.env.TWITTER_SECRET,
      }),
    ],
    callbacks: {
      async jwt({ token }) {
        const resUserRole = await fetch(`${SERVER}/api/get_user_role`);
        const userRole: string = resUserRole.ok
          ? await resUserRole.json()
          : ROLE.UNKNOWN_USER;
        token.userRole = userRole;

        return token;
      },
    },
  });

export default Auth;
