import Page from '@interfaces/page';

interface Chapter {
  background: string;
  number: number;
  pages: Page[];
  sound?: string;
  title?: string;
}

export default Chapter;
