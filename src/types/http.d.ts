import { Db, MongoClient } from 'mongodb';

declare module 'http' {
  interface IncomingMessage {
    db: Db;
    dbClient: MongoClient;
  }
}
