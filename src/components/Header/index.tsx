import * as React from 'react';

import MuiContainer from '@mui/material/Container';

import Logo from '@icons/Logo';

const Header: React.FC = () => (
  <MuiContainer
    component="header"
    sx={{
      display: 'flex',
      flexDirection: { mobile: 'column', md: 'row' },
      alignItems: 'center',
      bgcolor: 'background.paper',
      overflow: 'hidden',
      borderRadius: '12px',
      boxShadow: 1,
      fontWeight: 'bold',
    }}
  >
    <Logo />
  </MuiContainer>
);

export default Header;
