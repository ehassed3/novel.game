import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect, { Middleware } from 'next-connect';

import { client, clientPromise } from '@lib/dbadapter';

const db: Middleware<NextApiRequest, NextApiResponse> = async (
  req,
  _,
  next
) => {
  await clientPromise;

  req.dbClient = client;
  req.db = client.db('NovelDB');

  return next();
};

const middleware = nextConnect();

middleware.use(db);

export default middleware;
