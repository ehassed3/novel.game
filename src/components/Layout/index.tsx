import * as React from 'react';
import Head from 'next/head';

import Header from '@components/Header';

const Layout: React.FC = ({ children }) => (
  <>
    <Head>
      <title>Novel Game</title>
      <meta name="description" content="Novel Game — создавай и играй" />
    </Head>
    <Header />
    {children}
  </>
);

export default Layout;
