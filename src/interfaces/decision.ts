interface Required {
  relation?: number;
}

interface Decision {
  required?: Required;
  title: string;
}

export default Decision;
