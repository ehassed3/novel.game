import * as React from 'react';
import Head from 'next/head';

import MuiContainer from '@mui/material/Container';
import MuiTypography from '@mui/material/Typography';

const LibraryPage: React.FC = () => (
  <>
    <Head>
      <title>Novel Game — Library Page</title>
      <meta name="description" content="Novel Game — Library Page" />
    </Head>
    <MuiContainer
      component="main"
      sx={{
        display: 'flex',
        flexDirection: { mobile: 'column', md: 'row' },
        alignItems: 'center',
        bgcolor: 'background.paper',
        overflow: 'hidden',
        borderRadius: '12px',
        boxShadow: 1,
        fontWeight: 'bold',
      }}
    >
      <MuiTypography>Library Page</MuiTypography>
    </MuiContainer>
  </>
);

export default LibraryPage;
