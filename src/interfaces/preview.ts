interface Preview {
  description: string;
  image: string;
  title: string;
}

export default Preview;
