interface Author {
  description: string;
  title: string;
}

export default Author;
