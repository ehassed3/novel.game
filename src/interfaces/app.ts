import Chapter from '@interfaces/chapter';
import Character from '@interfaces/character';
import Preview from '@interfaces/preview';
import Protagonist from '@interfaces/protagonist';

interface App {
  chapters?: Chapter[];
  characters?: Character[];
  name: string;
  preview?: Preview;
  protagonist?: Protagonist;
}

export default App;
