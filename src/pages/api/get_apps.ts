import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';

const handler = nextConnect();

handler.get(async (_: NextApiRequest, res: NextApiResponse) => {
  return res.status(200).json([]);
});

export default handler;
