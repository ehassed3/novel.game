import React from 'react';
import useSWR from 'swr';

import MuiBox from '@mui/material/Box';
import MuiLinearProgress from '@mui/material/LinearProgress';

import App from '@interfaces/app';
import Progress from '@interfaces/progress';

interface AppProps {
  data?: App;
  loading: boolean;
  progress: Progress | null;
  setProgress: (progress: Progress | null) => void;
}

const AppStateContext = React.createContext<AppProps | null>(null);

interface Props {
  name?: string;
}

export const AppProvider: React.FC<Props> = ({ name, children }) => {
  const { data, isValidating: loading } = useSWR<App>(
    name ? `/api/get_app?name=${name}` : ''
  );

  const [progress, setProgress] = React.useState<Progress | null>(null);

  React.useEffect(() => {
    const storageProgress = localStorage.getItem(`${name}_progress`);

    if (storageProgress) {
      setProgress(JSON.parse(storageProgress));
    } else {
      setProgress({ chapterNumber: 1, pageNumber: 1 });
    }
  }, [name]);

  React.useEffect(() => {
    localStorage.setItem(`${name}_progress`, JSON.stringify(progress));
  }, [progress]);

  return (
    <AppStateContext.Provider value={{ data, loading, progress, setProgress }}>
      {(loading || !progress) && (
        <MuiBox
          sx={{
            alignItems: 'center',
            bgcolor: 'background.paper',
            bottom: 0,
            display: 'flex',
            justifyContent: 'center',
            left: 0,
            position: 'fixed',
            right: 0,
            top: 0,
          }}
        >
          <MuiLinearProgress variant="buffer" />
        </MuiBox>
      )}
      {children}
    </AppStateContext.Provider>
  );
};

export const useApp = (): AppProps => {
  const app = React.useContext(AppStateContext);

  if (!app) {
    throw new Error(
      '[Context App] Could not find required `app` object. <AppProvider> needs to exist in the component ancestry.'
    );
  }

  return app;
};
