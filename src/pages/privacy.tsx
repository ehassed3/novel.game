import * as React from 'react';
import Head from 'next/head';

import MuiContainer from '@mui/material/Container';
import MuiTypography from '@mui/material/Typography';

const PrivacyPage: React.FC = () => (
  <>
    <Head>
      <title>Novel Game — Privacy Page</title>
      <meta name="description" content="Novel Game — Privacy Page" />
    </Head>
    <MuiContainer
      component="main"
      sx={{
        display: 'flex',
        flexDirection: { xs: 'column', md: 'row' },
        alignItems: 'center',
        bgcolor: 'background.paper',
        overflow: 'hidden',
        borderRadius: '12px',
        boxShadow: 1,
        fontWeight: 'bold',
      }}
    >
      <MuiTypography>Privacy Page</MuiTypography>
    </MuiContainer>
  </>
);

export default PrivacyPage;
