interface Category {
  description: string;
  title: string;
}

export default Category;
