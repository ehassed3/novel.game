import * as React from 'react';
import { GetStaticPaths, GetStaticProps } from 'next';
import ErrorPage from 'next/error';
import Head from 'next/head';

import MuiContainer from '@mui/material/Container';
import MuiTypography from '@mui/material/Typography';

import SERVER from '@constants/server';

import Category from '@interfaces/category';

interface Props {
  category: Category | null;
}

const CategoryPage: React.FC<Props> = ({ category }) =>
  category ? (
    <>
      <Head>
        <title>Novel Game — {category.title}</title>
        <meta name="description" content={`Novel Game — ${category.title}`} />
      </Head>
      <MuiContainer
        component="main"
        sx={{
          display: 'flex',
          flexDirection: { mobile: 'column', md: 'row' },
          alignItems: 'center',
          bgcolor: 'background.paper',
          overflow: 'hidden',
          borderRadius: '12px',
          boxShadow: 1,
          fontWeight: 'bold',
        }}
      >
        <MuiTypography>{category.title}</MuiTypography>
        <MuiTypography>{category.description}</MuiTypography>
      </MuiContainer>
    </>
  ) : (
    <ErrorPage statusCode={404} />
  );

export const getStaticPaths: GetStaticPaths = async () => {
  // const resCategories = await fetch(`${SERVER}/api/get_categories/`);
  // const categories: string[] = resCategories.ok
  //   ? await resCategories.json()
  //   : [];

  // return {
  //   paths: categories.map((name) => ({ params: { name } })),
  //   fallback: false,
  // };

  return {
    paths: [],
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const resCategory = await fetch(
    `${SERVER}/api/get_category?name=${params?.name}`
  );
  const category: Category | null = resCategory.ok
    ? await resCategory.json()
    : null;

  return { props: { category } };
};

export default CategoryPage;
