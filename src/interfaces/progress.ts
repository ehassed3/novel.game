interface Progress {
  chapterNumber: number;
  pageNumber: number;
}

export default Progress;
