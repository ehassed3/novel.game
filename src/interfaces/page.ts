import Decision from '@interfaces/decision';
import Character from '@interfaces/character';

import INTERFACE from '@constants/interface';

interface Page {
  character?: Character;
  decisions?: Decision[];
  description: string;
  endSound?: string;
  interface: INTERFACE;
  number: string;
  startSound?: string;
  title?: string;
}

export default Page;
