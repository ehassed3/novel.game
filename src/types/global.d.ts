export declare global {
  namespace NodeJS {
    interface ProcessEnv {
      EMAIL_FROM: string;
      EMAIL_SERVER: string;
      FACEBOOK_ID: string;
      FACEBOOK_ID: string;
      FACEBOOK_SECRET: string;
      FACEBOOK_SECRET: string;
      GOOGLE_ID: string;
      GOOGLE_SECRET: string;
      MAILRU_ID: string;
      MAILRU_SECRET: string;
      MONGO_URL: string;
      NODE_ENV: 'development' | 'production';
      TWITTER_ID: string;
      TWITTER_SECRET: string;
      VK_ID: string;
      VK_SECRET: string;
    }
  }
}
